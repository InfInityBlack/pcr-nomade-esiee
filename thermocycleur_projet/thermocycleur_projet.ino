//pin values
const int heatPin = 5;//alim analogique du circuit/ pwm
const int lectureRm = A0;//lecture de la resistance de mesure
const int lectureTemp = A5;//lecture de la temperature ambiante
//const values
double ALPHA = 0.00165;

//default execution values
struct consigne {
  int TEMPERATURE;
  String TYPE;
  long DURATION;
};
const int DENATURE_TEMP_DEFAULT = 95;
const int ANNEALING_TEMP_DEFAULT = 60;
const int EXTENSION_TEMP_DEFAULT = 70;
const long MILLIS_DENATURE_DEFAULT = 60000;
const long MILLIS_ANNEALING_DEFAULT = 20000;
const long MILLIS_EXTENSION_DEFAULT = 35000;
int maxCycles = 30;
consigne DENATURE = {DENATURE_TEMP_DEFAULT,"DENATURE",MILLIS_DENATURE_DEFAULT};
consigne ANNEALING = {ANNEALING_TEMP_DEFAULT,"ANNEALING",MILLIS_ANNEALING_DEFAULT};
consigne EXTENSION = {EXTENSION_TEMP_DEFAULT,"EXTENSION",MILLIS_EXTENSION_DEFAULT};

//global values
int currentCycles = 0;
byte pwm;
double currTemp;
double V0;
double T0;
consigne currConsigne;

void setup() {
  // put your setup code here, to run once:
  pinMode(heatPin,OUTPUT);
  pinMode(lectureRm,INPUT);
  pinMode(lectureTemp,INPUT);
  Serial.begin(9600);
  V0 = getVoltMes();
  T0 = getAmbientTemp();
  //reachTemperature(30000, 95);
}

/**
 * Returns the current value of the measure resistance
 */
double getVoltMes(){
  double voltMes = analogRead(lectureRm);
  voltMes = 5*voltMes/1023;
  return voltMes;
}

/**
 * Reads the current ambient temperature from the TMP35 sensor
 */
double getAmbientTemp(){
  int reading = analogRead(lectureTemp);
  double volt = reading*5.0/1023;
  return (volt-0.5)*100;
}

/**
 * Reads  the current temperature measure by the measure resistance
 */
double readTemp(){
  currTemp = ((getVoltMes()/V0-1)/ALPHA + T0)/* + 2*T0*/;
  return currTemp;
  //return (getVoltMes()/V0)*ALPHA - T0;
}

void changePWM(char ch){
    pwm = map(ch,'0','9',0,255);
    analogWrite(heatPin,pwm);
}

/**
 * Creates a String representing the current state of the cycles
 */
void toString(){
  String res = "";
  res += currTemp;
  res += ",";
  res += currConsigne.TEMPERATURE;
  res += ",";
  res += currConsigne.TYPE;
  res += ",";
  res += currentCycles;
  res += ",";
  res += pwm;
  Serial.println(res);
}

/**
 * Sets the pwm to the given value
 */
void setPWM(byte val){
    pwm = val;
    analogWrite(heatPin,pwm);
}

/**
 * Reaches the given temperature
 */
void getToTemp(consigne cycle){
  consigne temporary = {cycle.TEMPERATURE,"HEATING",cycle.DURATION};
  currConsigne = temporary;
  double temp = cycle.TEMPERATURE;
  currTemp = readTemp();
  unsigned long startTime = millis();
  while(abs(readTemp() - temp) > 5.0){
    delay(200);
    long timeElapsed = millis() - startTime;
    if(timeElapsed >= 1000){
      toString();
      startTime = millis();
      timeElapsed = 0;
    }
    if((currTemp < temp || pwm == 0) && pwm < 255)
      setPWM(pwm+1);
    else
      setPWM(pwm-3);
    currTemp = readTemp();
  }
  delay(1000);
  toString();
  boolean a = abs(readTemp() - temp) > 2.0;
  delay(1000);
  toString();
  boolean b = abs(readTemp() - temp) > 2.0;
  if(a||b)
    getToTemp(cycle);
  currConsigne = cycle;
}

/**
 * Makes a small variation to the pwm to adjust for any gap between the target temperature and the current temperature
 */
void adjustTemp(consigne cycle){
  double temp = cycle.TEMPERATURE;
  if(abs(readTemp() - temp) > 2.0){
    if((readTemp()< temp || pwm == 0) && pwm < 254)
    setPWM(pwm+2);
    else{
      if(pwm <=255)
        setPWM(pwm-2);
    }
  }  
}

/**
 * Reaches the target temperature and stays around this temperature for the duration of the given cycle
 */
void reachTemperature(consigne cycle) {
  currConsigne = cycle;
  getToTemp(cycle);
  unsigned long startTime = millis();
  unsigned long adjustTempTimer = 0;
  unsigned long adjustTempTimerStart = millis();
  long timeElapsed = millis() - startTime;
  while (timeElapsed < cycle.DURATION) {
    timeElapsed = millis() - startTime;
    delay(500);
    if(adjustTempTimer - adjustTempTimerStart > 1000){
      adjustTemp(cycle);
      adjustTempTimer = 0;
      adjustTempTimerStart = millis();
    }
    currTemp = readTemp();
    toString();
    delay(500);
    }
    delay(210);
    timeElapsed = millis() - startTime;
  delay(90);
}

/**
 * Starts the cycles of heating using the configured consignes
 */
void cycleThrough(){
  currentCycles = 1;
  for(;currentCycles<=maxCycles;currentCycles++){
    reachTemperature(DENATURE);
    reachTemperature(ANNEALING);
    reachTemperature(EXTENSION);
  }
  currentCycles = 0;
}

/**
 * Changes the parameters of the cycles using a serial entry
 */
void pythonSetup(String conf[]){
    DENATURE.TEMPERATURE = conf[1].toDouble();
    DENATURE.DURATION = conf[2].toInt();
    ANNEALING.TEMPERATURE = conf[3].toDouble();
    ANNEALING.DURATION = conf[4].toInt();
    EXTENSION.TEMPERATURE = conf[5].toDouble();
    EXTENSION.DURATION = conf[6].toInt();
    maxCycles = conf[7].toInt();
    
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()){
    String message = Serial.readString();
    String conf[8];
    int i = 0;
    int index = 0;
    String next = "";
    while(i<=message.length()){
      if(i == message.length()){
        conf[index] = next;
        next = "";
        break;
      }
      if(message.charAt(i) == ','){
        conf[index] = next;
        next = "";
        index++;
      }   
      else{
        next += message.charAt(i);
      }
      i++;
    }
    char commande = conf[0].charAt(0);
    switch(commande){
      case 'S':
        cycleThrough();
        break;
      case 'C':
        pythonSetup(conf);
        break;
    }
  }
  
  delay(500);
}
