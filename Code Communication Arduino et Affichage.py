import datetime as dt
import time as temps
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as mpatches
import numpy as np
import random
import serial
from serial.serialutil import EIGHTBITS, PARITY_NONE, STOPBITS_ONE
from serial import *
from matplotlib.legend_handler import HandlerLine2D

#------------------------------------------------------------------------------------------


# Créer la fenêtre/figure initiale pour permettre le plotting
fig = plt.figure()
gx = fig.add_subplot(1, 1, 1)
ax = fig.add_subplot(2, 1, 1)
bx = fig.add_subplot(2, 1, 2)
y_range = [10, 120] # Range of possible Y values to display
ax.set_ylim(y_range)
bx.set_ylim(y_range)

nb_cycles_var = 1


axs = []
ays = []
bxs = []
bys = []

#------------------------------------------------------------------------------------------

# Définition de la fonction de lecture de la valeur envoyée par l'Arduino Uno

try :
    arduino=serial.Serial("COM3", 9600, EIGHTBITS, PARITY_NONE, STOPBITS_ONE,timeout=.1)
except :
    print("Vérifier le port utilisé")
    exit()

#------------------------------------------------------------------------------------------


# Valeurs des consignes à donner à l'Arduino

def envoi_consignes_arduino () :

    start_config = "C"

    tempDenature = input("Température de dénaturation: ")
    dureeDenature = input("Temps de la phase dénaturation: ")

    tempAnneal = input("Température d'hybridation: ")
    dureeAnneal = input("Temps de la phase d'hybridation: ")

    tempExt = input("Température d'élongation': ")
    dureeExt = input("Temps de la phase d'élongation: ")

    nb_cycles = input("Nombre de cycles d'amplification souhaités: ")

    string_to_arduino_config = start_config + "," + tempDenature + "," + dureeDenature + "," + tempAnneal + "," + dureeAnneal + "," + tempExt +  "," + dureeExt + "," + nb_cycles
    print(string_to_arduino_config)
    #print(type(string_to_arduino_config))

    arduino.write(string_to_arduino_config.encode('utf-8'))

    
    

#------------------------------------------------------------------------------------------

# Variables globales pour stocker les valeurs reçues par l'arduino

temperature_stock = 0
temperature_cible_stock = 0
nb_cycles_var_stock = 0

#------------------------------------------------------------------------------------------

# Démarrage du programme

start = 0
while start !=1 :
    print("Veuillez saisir les caractéristiques du programme PCR : \n\n")
    envoi_consignes_arduino ()
    
    choix_départ = input("Etes-vous sûr(e) des valeurs entrées? \n Y/N \n")
    if choix_départ == "Y" :
        start = 1
        time.sleep(0.5)
        arduino.write('S'.encode("utf-8"))
        
time.sleep(5)

temps_lancement = time.time()

#------------------------------------------------------------------------------------------

# Fonction qui va permettre de mettre à jour le graphe par appel périodique par la fonction FuncAnimation

def animate(i, axs, ays, bxs, bys, nb_cycles_var):

    
    arduino.flush()

    
    global temperature_stock
    global temperature_cible_stock
    global nb_cycles_var_stock
    
    
    print("Bytes en attente : ", arduino.in_waiting)

    
    donnee_recue = arduino.readline().decode('utf-8')

    #print(donnee_recue)
    #temperature = donnee_recue.split(",")[0]
    #temperature_stock = donnee_recue.split(",")[0]
    #temperature_cible = donnee_recue.split(",")[1]
    #nb_cycles_var = donnee_recue.split(",")[3]

    #-----------------------------------------------------------------------
    if (arduino.in_waiting >= 20):
        temperature = donnee_recue.split(",")[0]
        temperature_stock = donnee_recue.split(",")[0]
        temperature_cible = donnee_recue.split(",")[1]
        nb_cycles_var = donnee_recue.split(",")[3]
        
    if (arduino.in_waiting <= 20) : 
        temperature = temperature_stock
        temperature_cible = temperature_cible_stock
        nb_cycles_var = nb_cycles_var_stock
        arduino.reset_input_buffer()
    #-----------------------------------------------------------------------

    # Ajout du temps en abscisse et des valeurs de température calculées précédemment en ordonnée

    temps_actuel = time.time() - temps_lancement
    temps_actuel = int(temps_actuel)

    axs.append(temps_actuel)
    ays.append(float(temperature))

    bxs.append(temps_actuel)
    bys.append(float(temperature))

    #-----------------------------------------------------------------------
    
    # Limite des listes x et y aux 20 derniers indices pour ne conserver que les plus récents

    axs = axs[-25:]
    ays = ays[-25:]

    #-----------------------------------------------------------------------

    # Clear de la figure suivi de la création de la nouvelle prenant en compte les nouvelles valeurs de x et y => mise à jour

    ax.clear()
    ax.plot(axs, ays, label='Puce')
    bx.clear()
    bx.plot(bxs, bys, label='Puce')

    #-----------------------------------------------------------------------

    # Création des droites de consigne de température, des légendes et du cadre affichant le nb de cycles

    ax.axhline(y=int(temperature_cible),c="red",linewidth=1,zorder=0, linestyle='dashed', label = "Consigne")
    bx.axhline(y=int(temperature_cible),c="red",linewidth=1,zorder=0, linestyle='dashed', label = "Consigne")

    ax.legend(loc='upper right', shadow=True, fontsize='x-small')
    bx.legend(loc='upper right', shadow=True, fontsize='x-small')

    plt.text(0.8, 1.15,'Nombre de cycles: ' + str(nb_cycles_var), transform = ax.transAxes)
    
    #-----------------------------------------------------------------------

    # Format/Position/Titre/labels des abscisses et des ordonnées de la figure

    plt.xticks(rotation=45, ha='right')
    plt.suptitle('Température de la puce', y = 0.95, weight = 800)

    ax.set_ylim(y_range)
    bx.set_ylim(y_range)

    #-----------------------------------------------------------------------


# Appel de la fonction d'animation sur la figure de manière périodique

# Turn off axis lines and ticks of the big subplot

gx.spines['top'].set_color('none')
gx.spines['bottom'].set_color('none')
gx.spines['left'].set_color('none')
gx.spines['right'].set_color('none')
gx.tick_params(labelcolor='w', top=False, bottom=False, left=False, right=False)
gx.set_xlabel('Temps en secondes', labelpad = 20, weight = 500)
gx.set_ylabel('Température (degrés Celsius)', labelpad = 20, weight = 500)


ani = animation.FuncAnimation(fig, animate, fargs=(axs, ays, bxs, bys, nb_cycles_var), interval=1000)

plt.show()
