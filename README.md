# PCR nomade ESIEE

<h1>Files in this repository</h1>
<ul>
<li>Design spark files for the PCB</li>
<li>Design spark files for the case</li>
<li>Arduino files controling the ciruit</li>
<li>Python files needed to control the kit</li>
<\ul>
<br>
<h2>Guide d'utilisation python<\h2>
<p>Le code crée tout d'abord la fenêtre qui contiendra à terme les graphes de température générés en temps réel. La figure "gx" ne correspond pas
à un graphe, mais permettra en fin de code de visuellement créer un axe des ordonnées et des abscisses similaires pour les deux graphes 
(voir ligne 201- 207). On limite par ailleurs les valeurs de l'axe des ordonnées entre 10 et 120°C, puisque pour une PCR il est préférable de ne
pas dépasser les 100°C.

Le code tente d'initialiser la communication : s'il y parvient, le port est défini par la variable arduino ; sinon, il indique une erreur de
communication et arrête le programme.

La fonction envoi_consignes_arduino permet à l'utilisateur d'entrer les caractéristiques souhaitées pour les cycles de PCR (paliers de 
température, durées, nombre de cycles) puis d'envoyer ces informations à l'arduino.

Le programme utilise donc cette fonction, puis demande validation par oui ou non des caractéristiques saisies. Si l'utilisateur valide sa saisie, le 
programme envoie l'information de démarrage des cycles de PCR à l'arduino par un write 'S'; sinon, le programme redemande la saisie des informations.

Le programme se met en pause pendant 5 secondes le temps de recevoir les premières valeurs retournées par l'arduino.

La fonction animate, qui est la fonction principale du programme, va lire en entrée les données reçues depuis l'arduino, et les séparer en fonction
de l'information à isoler : la température actuelle, la température cible et le numéro du cycle en cours.

Il va ensuite ajouter ces données aux listes des ordonnées qui ne conservera pour l'un des deux graphes que les 25 dernières valeurs reçues.
Les valeurs en abscisses correspondantes sont aussi ajoutées et correspondent au temps en secondes depuis le lancement du programme de PCR.

Le programme, après avoir effacé les graphes affichés précédemment, utilise les nouvelles listes pour les abscisses et ordonnées pour afficher
les nouveaux graphes. Il va par ailleurs créer des lignes horizontales correspondant aux valeurs de température à atteindre, ainsi qu'une case de
texte indiquant le numéro du cycle en cours.

Il va en plus de cela ajouter les titres restants ainsi que fixer les limites de valeurs de l'axe des ordonnées.

Juste avant de lancer cette fonction, le programme se débarasse de tous les rebords du "graphe" global (le 'gx') afin de ne conserver visuellement
que les deux autres graphes, mais permettant néanmoins d'afficher des titres d'axes communs aux deux graphes principaux.

La fonction d'animation est enssuite appelée de manière périodique toutes les secondes.<\p>