from matplotlib.pyplot import arrow
import serial
import time
#from serial.serialutil import EIGHTBITS, PARITY_NONE, STOPBITS_ONE
#arduino = serial.Serial('COM3', 9600, EIGHTBITS, PARITY_NONE, STOPBITS_ONE,timeout=1,writeTimeout=.1)
arduino = serial.Serial(port='COM3', baudrate=9600, timeout=.1)

while True:
	arduino.write("S".encode())
	time.sleep(3)
	donnee_recue = arduino.readline().decode('utf-8') #the last bit gets rid of the new-line chars
	if (arduino.inWaiting() > 0):
		temperature = donnee_recue.split(",")[0]
		temperature_cible = donnee_recue.split(",")[1]
		etat_actuel = donnee_recue.split(",")[2]
		nb_cycles = donnee_recue.split(",")[3]
		print("Température : ", temperature)
		print("Température cible : ", temperature_cible)
		print("Etat actuel : ", etat_actuel)
		print("Nombre cycles : ", nb_cycles)
	else : 
		print("Connection problem.")
